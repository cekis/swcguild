
function playGame(){
    var startingMoney = document.getElementById("money").value;
    var playerMoney = startingMoney;
    var rolls = 0;
    var maxMoney = playerMoney;
    var maxRolls = 0;
    var dice1, dice2;

    while (playerMoney > 0) {
        dice1 = 1 + Math.floor(Math.random() * 6);
        dice2 = 1 + Math.floor(Math.random() * 6);

        if (dice1 + dice2 == 7) 
        {
            playerMoney += 4;
            rolls++;
            if (playerMoney > maxMoney)
            {
                maxMoney = playerMoney;
                maxRolls = rolls;
            }
        }
        else 
        {
            playerMoney--;
            rolls++;
        }
    }
    console.log("dice1: " + dice1);
    console.log("dice2: " + dice2);
    console.log("maxMoney: " + maxMoney);
    console.log("maxRolls: " + maxRolls);
    console.log("Rolls: " + rolls);
    document.getElementById("startingBet").value = startingMoney;
    document.getElementById("totalRolls").value = rolls;
    document.getElementById("highestAmount").value = maxMoney;
    document.getElementById("highestRolls").value = maxRolls;
    document.getElementById("playButton").value = "Play Again";
    showAllDivs();
}

function doLoadStuff(){		
    hideAllDivs();
}
	
function hideAllDivs(){
    var allDivs = document.getElementsByTagName("div");
    for (var i = 0; i < allDivs.length; i++){
        allDivs[i].style.display = "none";
    }
}

function showAllDivs(){
    var allDivs = document.getElementsByTagName("div");
    for (var i = 0; i < allDivs.length; i++){
        allDivs[i].style.display = "block";
    }
}