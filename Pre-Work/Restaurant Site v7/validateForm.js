function validateForm() {
    var isName = document.forms["contactUs"]["name"].value;
    var isEmail = document.forms["contactUs"]["email"].value;
    var isPhone = document.forms["contactUs"]["phone"].value;
    var isReason = document.forms["contactUs"]["reason"].value;
    var isInfo = document.forms["contactUs"]["addInfo"].value;
    var isMon = document.forms["contactUs"]["monCheck"].checked;
    var isTues = document.forms["contactUs"]["tuesCheck"].checked;
    var isWed = document.forms["contactUs"]["wedCheck"].checked;
    var isThurs = document.forms["contactUs"]["thursCheck"].checked;
    var isFri = document.forms["contactUs"]["friCheck"].checked;
    
    if((isName == null || isName == "") || ((isEmail == null || isEmail == "") && (isPhone == null || isPhone == ""))){
        alert("Please include your name and one form of contact information");
    }
    else if((isReason == "other") && (isInfo == null || isInfo == "")){
        alert("If Reason is 'other', please fill in the Addtional Info box");
    }
    else if(isMon == false && isTues == false && isWed == false && isThurs == false && isFri == false) {
        alert("Please select at least one best day to contact you");
    }
    else {
        alert("Your request has been submitted. Someone will contact you shortly");
        document.getElementById("contactUs").submit();
    }
}